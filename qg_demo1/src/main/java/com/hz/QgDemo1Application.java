package com.hz;

import com.hz.utils.RedisUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

@SpringBootApplication
public class QgDemo1Application {
    @Resource
    private RedisUtil redisUtil;
    public static void main(String[] args) {
        SpringApplication.run(QgDemo1Application.class, args);
    }

    @PostConstruct
    public void init(){
        redisUtil.setStr("goods_1",100+"");
    }

}
