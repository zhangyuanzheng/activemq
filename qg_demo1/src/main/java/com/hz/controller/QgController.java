package com.hz.controller;

import com.hz.utils.ActiveMQUtils;
import com.hz.utils.RedisUtil;
import com.hz.vo.Message;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class QgController {

    @Resource
    private ActiveMQUtils activeMQUtils;
    @Resource
    private RedisUtil redisUtil;
    final String MQ_KEY = "QG_MQ_MESSAGE123";

    @RequestMapping("/qg")
    public String qgActiveMQ(String userId,String goodsId){
        Message message = new Message();
        message.setGoodsId(goodsId);
        message.setUserId(userId);
        activeMQUtils.sendQueueMessage(MQ_KEY,message);
        return "用户"+userId+",排队中........";
    }



    @JmsListener(destination =MQ_KEY)
    public void qg( Message message){

        String userId = message.getUserId();
        String goodsId = message.getGoodsId();
     String lockkey = "lock_goods_"+goodsId;
        while(!redisUtil.lock(lockkey,60L)){
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

       String res = "";
       String flag =  redisUtil.getStr(userId+":"+goodsId);
       if(flag!=null&&"1".equals(flag))
       {

           System.out.println("用户"+userId+"已经抢购过此商品！！！！");
       }else{
           String stock = redisUtil.getStr("goods_"+goodsId);
           int stockgoods = Integer.parseInt(stock);
           if(stockgoods>=1)
           {
               stockgoods = stockgoods - 1;
               redisUtil.setStr( "goods_"+goodsId,stockgoods+"");
               //添加用户购买记录
               redisUtil.setStr("qg:"+userId+":"+goodsId,"1");
               res = "用户"+userId+"抢到一个商品!!!";
           }else{
               res = "库存不足!用户"+userId+"没有抢到";
           }
           System.out.println(res);
       }


     redisUtil.unLock(lockkey);//解锁
    }




}
